# CNN on MNIST DATASET
The MNIST database is available at http://yann.lecun.com/exdb/mnist/
I have uploaded a copy of the dataset in the MNIST direcctory.

The MNIST database is a dataset of handwritten digits. It has 60,000 training samples, and 10,000 test samples. Each image is represented by 28x28 pixels, each containing a value 0 - 255 with its grayscale value.

It is a subset of a larger set available from NIST. The digits have been size-normalized and centered in a fixed-size image.

It is a good database for people who want to try learning techniques and pattern recognition methods on real-world data while spending minimal efforts on preprocessing and formatting.

There are four files available, which contain separately train and test, and images and labels

## FILES
Main.pynb - CNN is implemented in this file

## DIRECTIONS TO RUN:
Download the entire repository.
Set the working directory to be the current folder
Run the Main.pynb file. It automatically uses the train data to train itself and shows the test accuracy on the test data

## DETAILS:
We implemented a 4 layer neural network consisting of two convolutional layers and two linear layers. The first linear one maps from a 50x4x4 dimensional space to a 500 dimensional one. After both convolutions and the first linear
layer we also want to apply ReLU non-linearities. Also we have implemented two linear layers and the ReLU non-linearities.